package starwars.org.ar.controllers;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import starwars.org.ar.dto.Satelite;
import starwars.org.ar.dto.Ship;
import starwars.org.ar.repositories.SateliteRepository;
import starwars.org.ar.services.IShipService;

@Validated
@RestController
@RequestMapping("/satelite")
public class SateliteController {

	private final String MSG_UPDATE = "Satelite %s was updated";
	private final String MSG_NOT_FOUND = "Satelite %s not found";

	@Autowired
	IShipService shipService;

	@Autowired
	SateliteRepository sateliteRepository;

	@PostMapping("/topsecret")
	public ResponseEntity<?> getShipAndUpdateSatellites(@Valid @RequestBody List<Satelite> satellites,
			BindingResult errors) throws Exception {
		sateliteRepository.updateSatellites(satellites);
		Ship ship = shipService.getShip();
		return new ResponseEntity<Ship>(ship, HttpStatus.OK);
	}

	@PostMapping("/topsecret/{name}")
	public ResponseEntity<?> updateSatelite(@PathVariable String name, @Valid @RequestBody Satelite satelite,
			BindingResult errors) throws Exception {
		satelite.setName(name);
		Satelite sateliteAux = sateliteRepository.updateSatelite(satelite);
		if (sateliteAux != null) {
			return new ResponseEntity<String>(String.format(MSG_UPDATE, name), HttpStatus.OK);
		} else {
			return new ResponseEntity<String>(String.format(MSG_NOT_FOUND, name), HttpStatus.NOT_FOUND);
		}

	}

	@GetMapping("/topsecret")
	public ResponseEntity<?> getShip() throws Exception {
		Ship ship = shipService.getShip();
		return new ResponseEntity<Ship>(ship, HttpStatus.OK);
	}

}
