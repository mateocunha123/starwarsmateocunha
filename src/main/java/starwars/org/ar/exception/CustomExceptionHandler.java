package starwars.org.ar.exception;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.apache.commons.math3.exception.TooManyEvaluationsException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

	private static final Logger logger = LoggerFactory.getLogger(CustomExceptionHandler.class);
	
	@ExceptionHandler({ ConstraintViolationException.class })
	public ResponseEntity<List<String>> handleConstraintViolation(ConstraintViolationException ex, WebRequest request) {
		List<String> listError = new ArrayList<String>();
		for (ConstraintViolation<?> element : ex.getConstraintViolations()) {
			String[] error = element.getPropertyPath().toString().split("\\.");
			listError.add("el campo " + error[error.length - 1] + " " + element.getMessage());
		}
		return new ResponseEntity<List<String>>(listError, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler({ Exception.class })
	public ResponseEntity<String> handleGenericExpeption(Exception e) {
		// devolveria un Internal Server Error, pero en el enunciado pide 404
		logger.error(e.getMessage(), e);
		return new ResponseEntity<String>("Error: " + e.getMessage(), HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler({ TooManyEvaluationsException.class })
	public ResponseEntity<String> handleGenericExpeption(TooManyEvaluationsException e) {
		// devolveria un Internal Server Error, pero en el enunciado pide 404
		logger.error(e.getMessage(), e);
		return new ResponseEntity<String>("Error: por favor revise que todos los campos de distancia sean correctos",
				HttpStatus.NOT_FOUND);
	}

}
