package starwars.org.ar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StarWarsMateoCunhaApplication {

	public static void main(String[] args) {
		SpringApplication.run(StarWarsMateoCunhaApplication.class, args);
	}

}
