package starwars.org.ar.services.implement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.math3.fitting.leastsquares.LevenbergMarquardtOptimizer;
import org.apache.commons.math3.fitting.leastsquares.LeastSquaresOptimizer.Optimum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lemmingapex.trilateration.NonLinearLeastSquaresSolver;
import com.lemmingapex.trilateration.TrilaterationFunction;

import starwars.org.ar.dto.Satelite;
import starwars.org.ar.dto.Ship;
import starwars.org.ar.repositories.SateliteRepository;
import starwars.org.ar.services.IShipService;

@Service
public class ShipServiceImpl implements IShipService {

	private static final Logger logger = LoggerFactory.getLogger(ShipServiceImpl.class);

	
	@Autowired
	SateliteRepository sateliteRepository;

	@Override
	public Ship getShip() throws Exception {
		Ship ship = new Ship();
		List<Satelite> satellites = new ArrayList<Satelite>(sateliteRepository.getSateliteList().values());
		// Hago esto aca porque en el ejercicio pide que la firma debe de ser de tal manera (De lo contrario lo haria
		// en getMessages)
		
		//listo los mensajes de los satelites
		String[] messagesAux;
		try {
			messagesAux = new String[satellites.get(0).getMessage().length];
			int count = 0;
			for (Satelite satelite : satellites) {
				count = 0;
				while (count < satelite.getMessage().length) {
					if (!satelite.getMessage()[count].isEmpty()) {
						messagesAux[count] = satelite.getMessage()[count];
					}
					count++;
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new Exception("No es posible crear el mensaje");
		}
		ship.setMessage(getMessages(messagesAux));
		if (satellites.size() < 2) {
			ship.setPosition(null);
		} else {
			ship.setPosition(getLocation(satellites));
		}
		return ship;
	}

	private String getMessages(String[] messagesAux) throws Exception {
		String message = "";
		for (String m : messagesAux) {
			message = message + m;
		}
		return message;
	}

	private Map<String, Float> getLocation(List<Satelite> satellites) {
		double[][] positions = new double[satellites.size()][2];
		double[] distances = new double[satellites.size()];
		for (int i = 0; i < satellites.size(); i++) {
			positions[i][0] = satellites.get(i).getCoordinateX();
			positions[i][1] = satellites.get(i).getCoordinateY();
			distances[i] = satellites.get(i).getDistance();

		}
		NonLinearLeastSquaresSolver solver = new NonLinearLeastSquaresSolver(
				new TrilaterationFunction(positions, distances), new LevenbergMarquardtOptimizer());
		Optimum optimum = solver.solve();
		double[] calculatedPosition = optimum.getPoint().toArray();
		Map<String, Float> positionsShip = new HashMap<String, Float>();
		positionsShip.put("X", (float) calculatedPosition[0]);
		positionsShip.put("Y", (float) calculatedPosition[1]);
		return positionsShip;
	}

}
