package starwars.org.ar.services;

import starwars.org.ar.dto.Ship;

public interface IShipService {
	public Ship getShip() throws Exception;
}
