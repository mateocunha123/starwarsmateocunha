package starwars.org.ar.repositories;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Repository;
import starwars.org.ar.dto.Satelite;

@Repository
@ConfigurationProperties(prefix = "satelite.repository")
public class SateliteRepository {

	// Hago un Map para que sea mas rapido la busqueda
	private Map<String, Satelite> sateliteList;

	public SateliteRepository() {
		sateliteList = new HashMap<String, Satelite>();
	}

	public void updateSatellites(List<Satelite> satellites) {
		for (Satelite satelite : satellites) {
			updateSatelite(satelite);
		}
	}

	public Satelite updateSatelite(Satelite satelite) {
		Satelite sateliteAux = null;
		sateliteAux = sateliteList.get(satelite.getName());
		if (sateliteAux != null) {
			sateliteAux.setMessage(satelite.getMessage());
			sateliteAux.setDistance(satelite.getDistance());
		}
		return sateliteAux;
	}

	public Map<String, Satelite> getSateliteList() {
		return sateliteList;
	}

	public void setSateliteList(Map<String, Satelite> sateliteList) {
		this.sateliteList = sateliteList;
	}

}
