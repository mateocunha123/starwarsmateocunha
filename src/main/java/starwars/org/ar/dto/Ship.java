package starwars.org.ar.dto;

import java.util.Map;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Ship {

	private String message;
	// Era esto o crear un objeto position (Ya que me pedian devolver un JSON deesta forma)
	private Map<String, Float> position;
}
