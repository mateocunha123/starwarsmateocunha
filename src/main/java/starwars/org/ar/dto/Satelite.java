package starwars.org.ar.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Satelite {

	private String name;
	private float distance;

	@NotNull(message = "se requiere")
	@NotEmpty(message = "no puede estar vacio")
	private String[] message;

	private float coordinateX;

	private float coordinateY;

	public Satelite() {
	}

	public Satelite(String name, float distance, String[] message, float coordinateX, float coordinateY) {
		this.name = name;
		this.distance = distance;
		this.message = message;
		this.coordinateX = coordinateX;
		this.coordinateY = coordinateY;
	}

}
