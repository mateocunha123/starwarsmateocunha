package starwars.org.ar;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import starwars.org.ar.config.StarWarsMateoCunhaApplicationTests;
import starwars.org.ar.dto.Satelite;
import starwars.org.ar.repositories.SateliteRepository;
import starwars.org.ar.services.IShipService;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = StarWarsMateoCunhaApplicationTests.class)
public class TestShipService {

	@Autowired
	private IShipService shipService;

	@Autowired
	private SateliteRepository sateliteRepository;

	@Test
	public void getShip() throws Exception {
		Satelite satelite1 = new Satelite();
		String[] message1 = { "hola ", "", "estas ", "" };
		satelite1.setName("kenobi");
		satelite1.setMessage(message1);
		satelite1.setDistance(100);

		Satelite satelite2 = new Satelite();
		String[] message2 = { "", "como ", "", "" };
		satelite2.setName("skywalker");
		satelite2.setMessage(message2);
		satelite2.setDistance(200);

		Satelite satelite3 = new Satelite();
		String[] message3 = { "", "", "", "?" };
		satelite3.setName("sato");
		satelite3.setMessage(message3);
		satelite3.setDistance(150);

		List<Satelite> listSatelite = new ArrayList<Satelite>();
		listSatelite.add(satelite1);
		listSatelite.add(satelite2);
		listSatelite.add(satelite3);

		sateliteRepository.updateSatellites(listSatelite);

		assertEquals("hola como estas ?", shipService.getShip().getMessage());
		assertEquals(shipService.getShip().getPosition().get("X"), (float) 55.068554);
		assertEquals(shipService.getShip().getPosition().get("Y"), (float) -17.690325);
	}

	@Test
	public void getShipErrorDistance() throws Exception {
		Satelite satelite1 = new Satelite();
		satelite1.setName("kenobi");
		satelite1.setDistance(100);

		Satelite satelite2 = new Satelite();
		satelite2.setName("skywalker");
		satelite2.setDistance(0);

		Satelite satelite3 = new Satelite();
		satelite3.setName("sato");
		satelite3.setDistance(0);

		List<Satelite> listSatelite = new ArrayList<Satelite>();
		listSatelite.add(satelite1);
		listSatelite.add(satelite2);
		listSatelite.add(satelite3);

		sateliteRepository.updateSatellites(listSatelite);

		Exception exception = null;
		try {
			shipService.getShip();
		} catch (Exception e) {
			exception = e;
		}

		assertNotNull(exception);
	}
}
