package starwars.org.ar.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import starwars.org.ar.StarWarsMateoCunhaApplication;
import starwars.org.ar.repositories.SateliteRepository;
import starwars.org.ar.services.implement.ShipServiceImpl;

@Configuration
@Import({ ShipServiceImpl.class, SateliteRepository.class })
@EnableAutoConfiguration
@ComponentScan("starwars.org.ar.*")
@PropertySource("classpath:server-config-test.properties")
public class StarWarsMateoCunhaApplicationTests {

	public static void main(String[] args) {
		SpringApplication.run(StarWarsMateoCunhaApplication.class, args);
	}
}
